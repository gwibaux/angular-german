import { ProductsAppPage } from './app.po';

describe('products-app App', () => {
  let page: ProductsAppPage;

  beforeEach(() => {
    page = new ProductsAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
