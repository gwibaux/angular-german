import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

import { AppComponent } from './app.component';
import { ServerComponent } from './components/server/server.component';
import { ServersComponent } from './components/servers/servers.component';
import { ServerElementComponent } from './components/server-element/server-element.component';
import { CookpitComponent } from './components/cookpit/cookpit.component';
import { BasicHighLightDirective } from './basic-highlight/basic-highlight.directive';  
import { BetterHighLightDirective } from './better-highlight/better-highlight.directive';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    ServerElementComponent,
    CookpitComponent,
    BasicHighLightDirective,
    BetterHighLightDirective    
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
