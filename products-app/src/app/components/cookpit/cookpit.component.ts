import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-cookpit',
    templateUrl: './cookpit.component.html',
    styleUrls: ['./cookpit.component.css']
})

export class CookpitComponent {
    @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
    @Output() blueprintCreated = new EventEmitter<{serverName: string, serverContent: string}>();
    newServerName = '';
    newServerContent= '';
    
    constructor() {}

    ngOnInit() {}

    onAddServer(nameInput) {
        console.log(nameInput.value);
        this.serverCreated.emit({
            serverName: this.newServerName, 
            serverContent: this.newServerContent
        });
    }

    onAddBluePrint() {
        this.blueprintCreated.emit({
            serverName: this.newServerName, 
            serverContent: this.newServerContent
        });
    }
}