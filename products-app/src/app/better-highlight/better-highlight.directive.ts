import { Directive, OnInit, Renderer2, ElementRef, HostListener } from '@angular/core';

@Directive({
    selector: '[appBetterHighLight]'
})

export class BetterHighLightDirective implements OnInit {
    constructor(private elRef:ElementRef, private renderer: Renderer2) {}

    ngOnInit() {
        //this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'black');        
    }

    @HostListener('mouseenter') mouseover (eventData: Event){
        this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'black');
    }

    @HostListener('mouseleave') mouseleave (eventData: Event){
        this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'transparent');
    }

}