import { Component } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DataStorageService } from '../shared/data-storage.service';
import { AuthService } from '../auth/auth.service';
import { SigninComponent } from '../auth/signin/signin.component';

@Component ({
    selector: "app-header",
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {
    constructor(private dataStorageService: DataStorageService,                
                private router: Router,
                private authOut: AuthService) {}


    onSaveData() {
        this.dataStorageService.storeProduct()
        .subscribe(
            (response: Response) => {
                console.log(response);
            }
        );
    }

    onPutProduct() {
        this.dataStorageService.putProduct()
            .subscribe(
                (response: Response) => {
                    console.log(response);
                }
            );
    }

    onFetchData() {
        this.dataStorageService.getProducts();
    }

    onLogOut() {
        this.authOut.logOutUser();        
        this.router.navigate(['/signin']);
    }
    
    

}