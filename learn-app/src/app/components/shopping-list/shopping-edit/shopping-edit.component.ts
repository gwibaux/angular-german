import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/subscription';

import { Ingredient } from '../../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list.service';

@Component ({
    selector: "app-shopping-edit",
    templateUrl: './shopping-edit.component.html'
})

export class ShoppingEditComponent implements OnInit, OnDestroy {
    //@ViewChild('nameInput') nameInputRef: ElementRef;
    //@ViewChild('amountInput') amountInputRef: ElementRef;
    subscription: Subscription;
    editMode = false;
    editingItemIndex : number;

    constructor(private slService: ShoppingListService) {}

    ngOnInit() {
        this.subscription = this.slService.startedEditing
            .subscribe(
                (index: number) => {
                    this.editingItemIndex = index;
                    this.editMode = true;
                }
            );
                
    }

    onAddItem(form: NgForm) {
        const value = form.value;
        const newIngredient = new Ingredient(value.name, value.amount);
        this.slService.addIngredient(newIngredient);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}