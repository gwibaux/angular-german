import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from "rxjs";


@Injectable()
export class AuthService {
    isLogged = false;
    baseUrl: string;

    constructor (private http: Http,
                 private router: Router) {
                     this.baseUrl = 'http://localhost:3000';
                 }

                 
    signupUser(name: string, password:string, res:string) {        
        return this.http.post(this.baseUrl + res, 
        {"nickname":name, "password":password})
        .map(response => response.json())
		.catch(this.handleError);
    }

    signInUser(name: string, password:string, res: string) {
        this.isLogged=true;        
        return this.http.post(this.baseUrl + res,
        {"nickname":name, "password":password})
        .map(response => response.json())        
        .catch(this.handleError);              
    }

    updateProduct() {
        
    }
    
    logOutUser() {
        console.log('helooooo');
        localStorage.removeItem;
        this.isLogged=false;
        this.router.navigate(['/signIn']);
    }

    logged() {
        return this.isLogged;
    }

    /**
	 * Handle error
	 * @param error
	 * @returns {any}
	 */
	private handleError(error: any) {
		console.error(error);
		return Observable.throw(error.json() || 'Server error');
	}

}