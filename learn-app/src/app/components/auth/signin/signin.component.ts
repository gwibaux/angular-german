import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Response } from '@angular/http';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component ({
    selector: "app-signin",
    templateUrl: './signin.component.html'    
})

export class SigninComponent implements OnInit {
    tokenAsigned = false;    

    constructor(private authservice: AuthService,
                private router: Router) { }

    ngOnInit() {        
    }

    onSignIn(form: NgForm) {
        const name = form.value.name;
        const password = form.value.password;
        this.authservice.signInUser(name, password, '/login')
        .subscribe(
            (response: any) => {                
                    localStorage.setItem('Authorization', response.token);
                    this.tokenAsigned = true;
                    console.log(this.tokenAsigned);
                    this.router.navigate(['/']);
                    console.log(response);
                // const resp = response.json();
                // console.log(response.json());
                // console.log(resp.token);
            }
        );
    }

    isAuthenticated() {
        return this.tokenAsigned;
    }

    

}