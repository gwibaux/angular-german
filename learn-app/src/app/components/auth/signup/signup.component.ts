import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Response } from '@angular/http';

import { AuthService } from '../auth.service';


@Component ({
    selector: "app-signup",
    templateUrl: './signup.component.html'    
})

export class SignupComponent implements OnInit {    

    constructor(private authService: AuthService) { }

    ngOnInit() {        
    }

    onSignUp(form: NgForm) {
        const name = form.value.name;
        const password = form.value.password;
        this.authService.signupUser(name, password,'/signup')
        .subscribe(
            (response: Response) => {
                console.log(response);
            }
        );       
    }

}