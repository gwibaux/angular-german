import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs";

import { ProductService } from '../products/product.service';
import { Product } from '../products/product.model';
import { SigninComponent } from '../auth/signin/signin.component';

@Injectable()
export class DataStorageService {
    baseUrl: string;
    constructor(private http: Http, 
                private productService: ProductService) {
                    this.baseUrl = 'http://localhost:3000/products';
                }

    storeProduct() {
        //console.log(this.productService.getProduct(0));
        let headers = this.getHeaders();
        return this.http.post(this.baseUrl, 
                      this.productService.getProduct(0), { headers: headers });
                      
    }

    getProducts() {
        let headers = this.getHeaders();        
        this.http.get(this.baseUrl, { headers: headers })
        .subscribe(
            (response: Response) => {
                const products: Product[] = response.json();
                this.productService.setProducts(products)
            }
        );
    }

    putProduct() {
        let headers = this.getHeaders();

        return this.http.put(this.baseUrl + '/5978c5dfb8a8330ec488d78c', {"seller":"DELL"}, { headers: headers })
			.map(response => response.json())
			.catch(this.handleError);
    }

    // putProduct() {
    //     return this.http.post(this.baseUrl + res, 
    //     {"nickname":name, "password":password})
    //     .map(response => response.json())
	// 	.catch(this.handleError);
    // }

    private getHeaders(): Headers {
        let headers = new Headers();
        let token = localStorage.getItem('Authorization');
        if (token)
            headers.append('Authorization', token);        
        return headers;
    }

    /**
	 * Handle error
	 * @param error
	 * @returns {any}
	 */
	private handleError(error: any) {
		console.error(error);
		return Observable.throw(error.json() || 'Server error');
	}

}