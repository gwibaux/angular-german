export class Product {
    public name: string;
    public category: string;
    public price: number;
    public seller: string;

    constructor(name: string, category: string, price: number, seller: string) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.seller= seller;                
    
    } 
}