import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Product } from './product.model';
//import { Ingredient } from '../shared/ingredient.model';
//import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class ProductService {
    productsChanged = new Subject<Product[]>();
    

    private products: Product[] = [
        new Product('Notebook L505 bis',
                   'Computacion bis',
                    15000000,
                    'Toshiba bis')
                ,
        new Product('GS50',
                    'Computacion',
                    12000,
                    'Lenovo')
    ];

    constructor() {}

    getProducts() {
        return this.products.slice();
    }

    getProduct(index: number) {
        return this.products[index];
    }

    setProducts(products: Product[]) {
        this.products = products;
        this.productsChanged.next(this.products.slice());
        console.log(this.products.slice());
    }

    // addIngredientsToShoppingList(ingredients: Ingredient[]) {
    //     this.slService.addIngredients(ingredients);
    // }

    // addRecipe(recipe: Recipe) {
    //     this.recipes.push(recipe);
    //     this.recipesChanged.next(this.recipes.slice());
    // }

    // updateRecipe(index:number, newRecipe: Recipe) {
    //     this.recipes[index] = newRecipe;
    //     this.recipesChanged.next(this.recipes.slice());
    // }

    // deleteRecipe(index: number) {
    //     this.recipes.splice(index, 1);
    //     this.recipesChanged.next(this.recipes.slice());
    // }

}