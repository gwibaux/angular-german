import { Injectable } from '@angular/core';
import { Http, Request, RequestOptionsArgs, Headers, Response, RequestOptions, ConnectionBackend } from "@angular/http";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import "rxjs/add/observable/empty";
import { Subject } from "rxjs/Subject";


@Injectable()
export class HttpInterceptor extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.request(url, options));
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.get(url, options));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.post(url, body, options));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.put(url, body, options));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return this.intercept(super.delete(url, options));
    }

    /**
     * Intercept any request
     * @param observable
     * @returns {Observable<Response>}
     */
    intercept(observable: Observable<Response>): Observable<Response> {

        return observable.catch((err, source) => {

            // If error is 401
            if (err.status == 401) {

                localStorage.clear();

                // Redirect to Login Page - Reload the page
                window.location.href = '/signin';
                return Observable.throw(err);
            }

            // Other error
            else {

                // Check if token has expired
                // if (localStorage.getItem('token')) {
                //     let jwtHelper: JwtHelper = new JwtHelper();
                //     let token = localStorage.getItem('token');
                //     if (jwtHelper.isTokenExpired(token)) {
                //         localStorage.clear();
                //         window.location.href = '/login';
                //         return Observable.throw(err);
                //     }
                // }
                return Observable.throw(err);
            }
                
        });
    }

}