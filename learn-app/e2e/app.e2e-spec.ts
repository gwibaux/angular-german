import { LearnAppPage } from './app.po';

describe('learn-app App', () => {
  let page: LearnAppPage;

  beforeEach(() => {
    page = new LearnAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
